#!/usr/bin/env bash

#set -e

INSTANCE_NAME=${INSTANCE_NAME:="instance-yyyymmdd-hhmmss"}
INSTANCE_PORT=${INSTANCE_PORT:="7860"}
INSTANCE_PROTOCOL=${INSTANCE_PROTOCOL:="HTTP"}
APP_NAME=${APP_NAME:="gradio"}
NETWORK="default" # this is being used for both the network and the subnets in an auto network setup
PROJECT_ID=$(gcloud config get-value project)
FQDN="${APP_NAME}.endpoints.${PROJECT_ID}.cloud.goog"
echo FQDN="$FQDN"


# Make sure the APIs are enabled (no harm re-running)
gcloud services enable compute.googleapis.com iap.googleapis.com cloudresourcemanager.googleapis.com

# Firewall Rule for Global Load Balancers - required
gcloud compute firewall-rules create ${NETWORK}-allow-load-balancer-healthchecks \
    --allow tcp:${INSTANCE_PORT} --source-ranges=35.191.0.0/16,130.211.0.0/22 \
    --network ${NETWORK} 


# Determine the zone of the instance
ZONE=$(gcloud compute instances list --filter='name=('${INSTANCE_NAME}')' --format='value(zone)')
if [ -z "$ZONE" ]
then
    echo >&2 "Cannot determine zone of the instance. Please check your instance name:"
    gcloud compute instances list
else
    echo ZONE=${ZONE}
fi


# Create a new unmanaged instance group
gcloud compute instance-groups unmanaged create ${APP_NAME}-group --zone=${ZONE}
gcloud compute instance-groups unmanaged add-instances ${APP_NAME}-group --zone=${ZONE} --instances=${INSTANCE_NAME}
gcloud compute instance-groups unmanaged set-named-ports ${APP_NAME}-group \
    --zone=${ZONE} \
    --named-ports="tcp${INSTANCE_PORT}:${INSTANCE_PORT}"


# Healthcheck. Use TCP healthcheck to avoid redirection (non-200) status failing healthcheck for now
gcloud compute health-checks create tcp ${APP_NAME}-healthcheck --port ${INSTANCE_PORT}

# Backend service for instance group
gcloud compute backend-services create ${APP_NAME}-backend \
    --global --health-checks=${APP_NAME}-healthcheck \
    --port-name=tcp${INSTANCE_PORT} \
    --protocol=${INSTANCE_PROTOCOL} \
    --load-balancing-scheme=EXTERNAL_MANAGED \
    --timeout=2147483647
gcloud compute backend-services add-backend ${APP_NAME}-backend \
    --instance-group=${APP_NAME}-group \
    --instance-group-zone=${ZONE} \
    --global

# gcloud compute backend-services delete ${APP_NAME}-backend --global

## IP and DNS

gcloud compute addresses create ${APP_NAME}-ip --global
IP_ADDRESS=$(gcloud compute addresses describe ${APP_NAME}-ip --global --format="value(address)")
echo IP_ADDRESS=$IP_ADDRESS

# Map the FQDN to the IP address
# Alternatives include the use of DNS or services like xip.io
cat <<EOF > ${APP_NAME}-openapi.yaml
swagger: "2.0"
info:
  description: "app"
  title: "app"
  version: "1.0.0"
host: "${FQDN}"
x-google-endpoints:
- name: "${FQDN}"
  target: "$IP_ADDRESS"
paths: {}
EOF
gcloud endpoints services deploy ${APP_NAME}-openapi.yaml
rm ${APP_NAME}-openapi.yaml

# gcloud endpoints services delete "${FQDN}" 

## Add LB Frontend

# Set up the URL map
gcloud compute url-maps create "${APP_NAME}-https-load-balancer" --default-service="${APP_NAME}-backend"
# gcloud compute url-maps delete  "${APP_NAME}-https-load-balancer"

gcloud compute ssl-certificates create "${APP_NAME}-ssl-cert" --domains "${FQDN}"

# gcloud compute target-https-proxies delete "${APP_NAME}-httpsproxy" 

gcloud compute target-https-proxies create "${APP_NAME}-httpsproxy" --url-map="${APP_NAME}-https-load-balancer" --ssl-certificates="${APP_NAME}-ssl-cert" 


# gcloud compute forwarding-rules delete ${INSTANCE_NAME}-https-forwardingrule --global
gcloud compute forwarding-rules create ${APP_NAME}-https-forwardingrule \
    --network-tier=PREMIUM \
    --target-https-proxy=${APP_NAME}-httpsproxy \
    --global --ports=443 \
    --address=${APP_NAME}-ip \
    --load-balancing-scheme=EXTERNAL_MANAGED
    # --load-balancing-scheme=EXTERNAL

## Handle http-to-https redirect
cat  <<EOF | gcloud compute url-maps import "${APP_NAME}-http-load-balancer" --global --source=-
kind: compute#urlMap
name: ${APP_NAME}-http-load-balancer
defaultUrlRedirect:
  redirectResponseCode: MOVED_PERMANENTLY_DEFAULT
  httpsRedirect: True
EOF


# gcloud compute url-maps describe "${APP_NAME}-http-load-balancer"  --global

gcloud compute target-http-proxies create "${APP_NAME}-httpproxy" --url-map="${APP_NAME}-http-load-balancer"

gcloud compute forwarding-rules create ${APP_NAME}-http-forwardingrule \
    --load-balancing-scheme=EXTERNAL_MANAGED \
    --network-tier=PREMIUM \
    --target-http-proxy=${APP_NAME}-httpproxy \
    --global --ports=80 --address=${APP_NAME}-ip

printf "\n\n\nThe URL will be at https://${FQDN} (may take 10-20min) \n\n\n"

